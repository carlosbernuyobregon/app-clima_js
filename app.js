window.onload = () => {
    //latitud y longitud
    let lat, lon;

    let temperaturaValor = document.getElementById('temperatura-valor');
    let temperaturaDescripcion = document.getElementById('temperatura-descripcion');

    let ubicacion = document.getElementById('ubicacion');
    let icono = document.getElementById('icon');

    let velocidadViento = document.getElementById('velocidad-viento');

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(posicion => {
            lat = posicion.coords.latitude;
            lon = posicion.coords.longitude;
            //ubicacion actual
            // const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=d19e7e5266abb1c6f2d31e229bd59953`;

            //ubicacion por ciudad
            const url = `https://api.openweathermap.org/data/2.5/weather?q=Barcelona&lang=es&units=metric&appid=d19e7e5266abb1c6f2d31e229bd59953`;
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    let temp = Math.round(data.main.temp);
                    let tempDescripcion = data.weather[0].description;
                    let city = data.name;
                    let viento = data.wind.speed;
                    //
                    temperaturaValor.textContent = `${temp} °C`;
                    temperaturaDescripcion.textContent = tempDescripcion.toUpperCase();
                    ubicacion.textContent = city;
                    velocidadViento.textContent = `${viento} m/s`;
                    console.log(data);
                    switch (data.weather[0].main) {
                        case 'Thunderstorm':
                            icono.src = 'animated/thunder.svg'
                            console.log('TORMENTA');
                            break;
                        case 'Drizzle':
                            icono.src = 'animated/rainy-2.svg'
                            console.log('LLOVIZNA');
                            break;
                        case 'Rain':
                            icono.src = 'animated/rainy-7.svg'
                            console.log('LLUVIA');
                            break;
                        case 'Snow':
                            icono.src = 'animated/snowy-6.svg'
                            console.log('NIEVE');
                            break;
                        case 'Clear':
                            icono.src = 'animated/day.svg'
                            console.log('LIMPIO');
                            break;
                        case 'Atmosphere':
                            icono.src = 'animated/weather.svg'
                            console.log('ATMOSFERA');
                            break;
                        case 'Clouds':
                            icono.src = 'animated/cloudy-day-1.svg'
                            console.log('NUBES');
                            break;
                        default:
                            icono.src = 'animated/cloudy-day-1.svg'
                            console.log('por defecto');
                    }
                })
                .catch(error => console.log(error));
        });
    }
}